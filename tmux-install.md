# From source tarball
tmux requires two libraries to be available:

- libevent
- ncurses

```sudo apt-get install libevent-dev```

```sudo apt-get install libncurses5-dev libncursesw5-dev```

In addition, tmux requires a C compiler, make, yacc (or bison) and pkg-config.

## Install C compiler

```sudo apt install build-essential```

## Install LEX

```sudo apt-get install flex```

## Install YACC

```sudo apt-get install bison```

# From version control
To get and build the latest from version control - note that this requires autoconf, automake and pkg-config:

```
git clone https://github.com/tmux/tmux.git
cd tmux
sh autogen.sh
./configure && make
```
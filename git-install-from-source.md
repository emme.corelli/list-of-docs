# HOW TO INSTALL LAST VERSION

you need to have the following libraries that Git depends on:
```sudo apt-get install dh-autoreconf libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev make```

If you’re using a Debian-based distribution (Debian/Ubuntu/Ubuntu-derivatives), you also need the install-info package:
```sudo apt-get install install-info```

When you have all the necessary dependencies, you can go ahead and grab the latest tagged release tarball from several places. You can get it via the kernel.org site, at https://www.kernel.org/pub/software/scm/git, or the mirror on the GitHub website, at https://github.com/git/git/tags.

```
$ sudo apt install make
$ tar -zxf git-2.8.0.tar.gz
$ cd git-2.8.0
$ make configure
$ ./configure --prefix=/usr
$ make all doc info
$ sudo make install install-doc install-html install-info
```

NB.
If you don't want the DOCs don't add **doc info** after **make all** and don't add **install-doc install-html install-info** after **sudo make install**.

Check the installation
```git --version``` 

After this is done, you can also get Git via Git itself for updates:
```git clone https://git.kernel.org/pub/scm/git/git.git```
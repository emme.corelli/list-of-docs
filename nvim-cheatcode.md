# nvimtree

a - Add new folder/file
r - Rename folder/file
d - Delete folder/file
<SPACE> Open folder/Edit file

E - (expand_all) per espandere l'intera struttura dei file partendo dalla cartella principale (area di lavoro)
W - (collapse_all) per chiudere tutte le cartelle aperte, a partire da quella principale

f - (find) per aprire la ricerca interattiva dei file a cui si possono applicare i filtri di ricerca
F - per chiudere la ricerca interattiva, è necessario riportare il buffer a NORMAL con ESC prima di chiudere la ricerca con F.

# nvim

<SPACE> c h     - show cheat sheet h
<SPACE> h       - horizontal TERM 
:sp             - horizontal split
:vs             - veritcal   split

## Editing Commands I

i - Enter Insert mode before the cursor
a - Enter Insert mode after the cursor
I - Enter Insert mode at beginning of the line
A - Enter Insert mode at the end of the line
o - Insert a new line below the current line and enter Insert mode
O - Insert a new line above the current line and enter Insert mode

## Editing Commands II
dd - Delete the current line
D - Delete from the cursor to the end of the line
C - Change from the cursor to the end of the line and enter Insert mode
u - Undo the last change
cw - Change from cursor to end of word and enter Insert mode
cb - Change from cursor to beginning of word and enter Insert mode

## Cut and paste:
Position the cursor where you want to begin cutting.
Press v to select characters, or uppercase V to select whole lines, or Ctrl-v to select rectangular blocks (use Ctrl-q if Ctrl-v is mapped to paste).
Move the cursor to the end of what you want to cut.
Press d to cut (or y to copy).
Move to where you would like to paste.
Press P to paste before the cursor, or p to paste after.
Copy and paste is performed with the same steps except for step 4 where you would press y instead of d:

d stands for delete in Vim, which in other editors is usually called cut
y stands for yank in Vim, which in other editors is usually called copy

## Undo and redo commands
u			Undo
CTRL-R			Redo
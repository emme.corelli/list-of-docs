sudo apt-get update
sudo apt-get install zsh -y
chsh -s /usr/bin/zsh

 Here you can do what is specified by typing the character given at the start of every line. You can quit, go in the main menu and configure the shell your self or you can go for the recommended configuration for the shell by pressing 2, which is what we have done.

# binding keys

In $HOME/.zshrc add to the end of file
```
bindkey -s "^L" '~'         # ctrl+ì -> ~
bindkey -s "^G" '`'         # ctrl+' -> `

export PATH=$HOME/.local/bin:$PATH
```

A good terminal needs some good fonts, we’d use Terminess nerd font to make our terminal look awesome, which can be downloaded here. Once downloaded, extract and move them to ~/.local/share/fonts to make them available for the current user or to /usr/share/fonts to be available for all the users.

```
tar -xvf Terminess.zip
mv *.ttf ~/.local/share/fonts
```
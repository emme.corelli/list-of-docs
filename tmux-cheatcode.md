## Windows

Ctrl + b c          Create window
Ctrl + b &          Close current window
Ctrl + b 0 ... 9    Switch/select window by number

## Panes

Ctrl + b %          Split pane with horizontal layout
Ctrl + b "          Split pane with vertical layou
Ctrl + b z          Toggle pane zoom

## Copy Mode


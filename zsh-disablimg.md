# Disabling zsh
Simply run **chsh** and select whatever shell you were using before.

# Disabling only oh-my-zsh
Check if ~/.zshrc.pre-oh-my-zsh exists. It probably does. (This file will have been created when the oh-my-zsh installation script moved your previous .zshrc out of the way. .zshrc is a startup file of zsh, similar to .bashrc for bash.)
- If it does, do **mv ~/.zshrc ~/.zshrc.oh-my-zsh**. This will put the oh-my-zsh-created .zshrc out of the way, so we can restore the original, by doing **mv ~/.zshrc.pre-oh-my-zsh ~/.zshrc**.
- If it does not exist, open ~/.zshrc in a text editor. Find the line that says source $ZSH/.oh-my-zsh and either comment it out or remove it. This will disable the initialization of oh-my-zsh.
## Build prerequisites
General requirements (see #1469):

- Clang or GCC version 4.9+
- CMake version 3.10+, built with TLS/SSL support
- Optional: Get the latest CMake from an installer or the Python package (pip install cmake)

Ubuntu / Debian
- sudo apt-get install ninja-build gettext cmake unzip curl

## Install neovim

```
git clone https://github.com/neovim/neovim
cd neovim && make CMAKE_BUILD_TYPE=RelWithDebInfo
```

If you want the stable release, also run ```git checkout stable```.
If you want to install to a custom location, ```set CMAKE_INSTALL_PREFIX```. See also Installing Neovim.

On Debian/Ubuntu, instead of installing files directly with sudo make install, you can run ```cd build && cpack -G DEB && sudo dpkg -i nvim-linux64.deb``` to build DEB-package and install it. This should help ensuring the clean removal of installed files.

Default install location is /usr/local

## install ripgrep
sudo dpkg -i ripgrep_13.0.0_amd64.deb

## install NvChad 
git clone https://github.com/NvChad/NvChad ~/.config/nvim --depth 1 && nvim